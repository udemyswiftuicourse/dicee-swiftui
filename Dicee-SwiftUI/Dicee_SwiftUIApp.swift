//
//  Dicee_SwiftUIApp.swift
//  Dicee-SwiftUI
//
//  Created by Sergio Andres Rodriguez Castillo on 09/01/24.
//

import SwiftUI

@main
struct Dicee_SwiftUIApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
